import requests
from fabric.operations import local as lrun
from fabric.api import lcd, settings, sudo
from fabric.state import env
import time


env.hosts = ["localhost"]
path = "/home/pi/rpi"
server = "http://controme-main.appspot.com"

def update():
    result = lrun("ifconfig | grep ^eth0.*HWaddr", capture=True)
    macaddr = result.rsplit(' ', 1)[1].replace(':', '-')
    try:
        r = requests.get("%s/get/update/%s/" % (server, macaddr))
    except:
        return
    if r.status_code == 200:

        with lcd(path) and settings(warn_only=True):
            result = lrun("git show HEAD", capture=True)
            head = result.split("\n")[0].split(' ')[1]

            result = lrun("git pull", capture=True)
            if result.return_code != 0:
                if 'fatal: loose object' in result:
                    while not revert():
                        time.sleep(1)

            lrun("fab -f config/fab_upgrade.py upgrade:%s" % head)

    else:
        with lcd(path):
            result = lrun("git fsck", capture=True)
            if "fatal: loose object" in result:
                while not revert():
                    time.sleep(1)


def revert():
    print "trying to revert ..."
    with lcd("/home/pi/"):
        lrun("cp rpi/rpi/aeskey.py .")
        lrun("cp rpi/rpi/secret_key.py .")
        lrun("cp rpi/rpi/verification_code.py .")
        lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf stop all")
        lrun("rm -rf rpi")
        lrun("git clone https://hcerny@bitbucket.org/hcerny/cms_upgrade.git rpi/")
        lrun("mv aeskey.py rpi/rpi/")
        lrun("mv secret_key.py rpi/rpi/")
        lrun("mv verification_code.py rpi/rpi/")
        lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf start all")
    return True
        #result = lrun('git reset --hard @{1}')
