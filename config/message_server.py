import zmq
from zmq.core.error import ZMQError
import requests
from rpi.aeshelper import aes_encrypt, aes_decrypt
from fabric.api import local
import sys
from datetime import datetime


class MessageServer():

    def __init__(self):

        self.server = "http://controme-main.appspot.com"

        self.context = zmq.Context()

        self.handler_insocket = self.context.socket(zmq.DEALER)
        self.handler_insocket.bind("tcp://127.0.0.1:%s" % 5557)

        self.handler_outsocket = self.context.socket(zmq.DEALER)
        self.handler_outsocket.connect("tcp://127.0.0.1:%s" % 5558)

        self.poller = zmq.Poller()
        self.poller.register(self.handler_insocket, zmq.POLLIN)

        self.network = "None"

    def run(self):
        print "msgsrv ready"

        try:
            
            result = local("ifconfig | grep ^eth0.*HWaddr", capture=True)
            macaddr = result.rsplit(' ', 1)[1].replace(':', '-')
            
            while True:

                socks = dict(self.poller.poll(10))

                if self.handler_insocket in socks and socks[self.handler_insocket] == zmq.POLLIN:
                    try:
                        msg = self.handler_insocket.recv_json()
                    except ZMQError, e:
                        if e.errno == zmq.EAGAIN:  # no message ready
                            pass
                        else:
                            raise e
                    else:
                        self.handler_insocket.send_json({'ack': True})

                        network_id = msg.keys()[0]
                        self.network = network_id
                        try:
                            import json
                            d = aes_encrypt(json.dumps(msg.values()[0]))
                            r = requests.post("%s/set/zwave/%s/%s/" % (self.server, macaddr, network_id),
                                              data="data=%s" % d)
                        except (requests.ConnectionError, requests.Timeout):
                            print "connectionerror"

                        try:
                            r = requests.get("%s/get/zwave/%s/" % (self.server, network_id))
                            if r.status_code == 200:
                                import json
                                self.handler_outsocket.send_json(json.loads(aes_decrypt(r.content).rsplit('}', 1)[0] + '}'))
                                try:
                                    self.handler_outsocket.recv_json()
                                except ZMQError, e:
                                    if e.errno == zmq.EAGAIN:  # no message ready
                                        pass
                                    else:
                                        raise e
                        except (requests.ConnectionError, requests.Timeout):
                            print "connectionerror"

        except (KeyboardInterrupt, Exception) as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            err = "%s - Exception %s / %s in msgsrv.run: %s" % (str(datetime.now()), exc_type, exc_obj, exc_tb.tb_lineno)
            print err
            self.handler_insocket.close(linger=0)
            self.handler_outsocket.close(linger=0)
            self.context.term()

            try:
                requests.post("%s/get/zwave/%s/" % (self.server, self.network), data=err)
            except Exception:
                pass


def main():
    ms = MessageServer()
    ms.run()

if __name__ == "__main__":
    main()
