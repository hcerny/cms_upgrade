from fabric.api import local


def check_processes():
    result = local("sudo supervisorctl -c /etc/supervisor/supervisord.conf status", capture=True)
    for line in result.split('\n'):
        if 'STOPPED' in line or 'EXITED' in line or 'FATAL' in line:
            local("sudo supervisorctl -c /etc/supervisor/supervisord.conf start %s" % line.split(' ')[0])


if __name__ == '__main__':
    check_processes()