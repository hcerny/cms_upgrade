import cuisine
from fabric.operations import local as lrun
from fabric.api import lcd, settings, sudo
import requests
from fabric.state import env


env.hosts = ['localhost']
path = "/home/pi/rpi"
REMOTE_HOST = "http://controme-main.appspot.com"

def setup_virtualenv():
    pass


def install_packages():
    cuisine.package_ensure_apt("nginx")  # 1.2.1
    cuisine.package_ensure_apt("postgresql-9.1")
    cuisine.package_ensure_apt("supervisor")  # 3.0
    cuisine.package_ensure_apt("rabbitmq-server")  # 2.8.4 ... lokal 3.3.0
    cuisine.package_ensure_apt("python-pip")
    cuisine.package_ensure_apt("memcached")  # 1.4.13
    cuisine.python_package_ensure_pip(r="%s/config/requirements.txt")
    # ufw?
    # python-openzwave


def install_software():
    lrun("cd %s && git clone https://bitbucket.org/hcerny/cms_upgrade.git ." % path)


def configure_uwsgi():
    cuisine.sudo("cp %s/config/uwsgi-rpi.ini /etc/uwsgi/apps-available/rpi.ini")
    cuisine.sudo("ln -s /etc/uwsgi/apps-available/rpi.ini /etc/uwsgi/apps-enabled/rpi.ini")


def configure_nginx():
    cuisine.sudo("cp %s/config/nginx.conf /etc/nginx/")
    cuisine.sudo("service nginx reload")


def configure_supervisor():
    cuisine.sudo("cp %s/config/supervisor-messaging.conf /etc/supervisor/conf.d/messaging.conf")
    cuisine.sudo("cp %s/config/supervisor-uwsgi.conf /etc/supervisor/conf.d/uwsgi.conf")
    cuisine.sudo("cp %s/config/supervisor-celery.conf /etc/supervisor/conf.d/celery.conf")
    cuisine.sudo("supervisorctl start zwavehandler messageserver uwsgi celery celerybeat")


def get_gw_cryptkeys():
    # dafuer fehlt die server schnittstelle noch
    pass


def upgrade(old_head):

    with lcd(path) and settings(warn_only=True):
        commitlist = lrun("git log %s..HEAD | grep ^commit" % old_head, capture=True)

    commits = []
    for commit in commitlist.split('\n'):
        try:
            commits.append(commit.split(' ')[1])
        except IndexError:
            continue

    for commit in commits:
        try:
            globals()["upgrade%s" % commit[:8]]()
        except Exception:
            pass
    
    last = commits[0] if len(commits) else None
    if last:
        result = lrun("ifconfig | grep ^eth0.*HWaddr", capture=True)
        macaddr = result.rsplit(' ', 1)[1].replace(':', '-')
        requests.get("%s/get/update/%s/?ver=%s" % (REMOTE_HOST, macaddr, last))

    lrun("sudo cp /home/pi/rpi/config/supervisor-celery.conf /etc/supervisor/conf.d/celery.conf")
    result = lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf status | grep celerybeat", capture=True)
    if "FATAL" in result:
        lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart celerybeat")

    lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart messageserver zwavehandler")
    
    try:
        result = local("cat /etc/crontab | grep svhc", capture=True)
    except:
        local("""echo '*/15 * * * * pi /usr/bin/python /home/pi/rpi/config/svhc.py' | sudo tee -a /etc/crontab""")
   

def restart_services():
    #lrun("sudo service nginx reload")
    lrun("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart messageserver zwavehandler")


def stop():
    cuisine.sudo("service nginx stop")
    cuisine.sudo("supervisorctl -c /etc/supervisor/supervisord.conf stop zwavehandler messageserver uwsgi celery celerybeat")
