from __future__ import absolute_import

import os
from celery import Celery
from django.conf import settings
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'rpi.settings')

app = Celery('rpi', broker="amqp://localhost")

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS, related_name='tasks')

app.conf.update(
    CELERYBEAT_SCHEDULE={
        'send-setup-every-10-minutes': {
            'task': 'dataslave.tasks.send_setup',
            'schedule': crontab(minute="*/10"),
        },
        'retrieve-upgrade-instructions-every-10-minutes': {
            'task': 'dataslave.tasks.do_update',
            'schedule': crontab(minute="*/10"),
        },
    },
    CELERYBEAT_PID_FILE="/var/run/celerybeat.pid",
)

CELERY_TIMEZONE = 'UTC'
