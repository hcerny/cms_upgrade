from __future__ import absolute_import

from celery import shared_task
import requests
from django.core.cache import cache
import logging
from fabric.api import local, sudo
from rpi.aeshelper import aes_encrypt, aes_decrypt
from fabric.state import env

url = "http://controme-main.appspot.com"
env.hosts = ["localhost"]

@shared_task
def get_gw_outputs(macaddr):
    logging.warning("started delayed task get_gw_outputs")
    r = requests.get("%s/get/%s/all/" % (url, macaddr))
    if r.status_code == 200:
        logging.warning(aes_decrypt(r.content))
        cache.set("get_%s" % macaddr.lower(), aes_decrypt(r.content), 600)


@shared_task
def get_gw_backup(macaddr, version):
    logging.warning("started delayed task get_gw_backup")
    r = requests.get("%s/get/%s/%s/" % (url, macaddr, version))
    if r.status_code == 200:
        logging.warning(aes_decrypt(r.content))
        cache.set("backup_%s" % macaddr.lower(), aes_decrypt(r.content), 600)


@shared_task
def get_gw_setting(macaddr, version):
    logging.warning("started delayed task get_gw_setting")
    r = requests.get("%s/get/%s/setting/%s/" % (url, macaddr, version))
    if r.status_code == 200:
        logging.warning(aes_decrypt(r.content))
        cache.set("setting_%s" % macaddr.lower(), aes_decrypt(r.content), 600)


@shared_task
def send_setup():
    import socket
    import fcntl
    import struct
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    lan_ip = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', "eth0"))[20:24])
    import json
    result = local("ifconfig | grep ^eth0.*HWaddr", capture=True)
    macaddr = result.rsplit(' ', 1)[1].replace(':', '-')
    try:
        r = requests.post("%s/get/update/%s/" % (url, macaddr), data="data=%s" % aes_encrypt(json.dumps({'ip': lan_ip, 'ver': 1})))
        if r.status_code == 404:
            local("sudo supervisorctl -c /etc/supervisor/supervisord.conf restart messageserver zwavehandler")
        elif r.status_code == 418:
            local("sudo shutdown -r now")
    except:
        pass


@shared_task
def do_update():
    local("fab -f /home/pi/rpi/config/fab_update.py update")
