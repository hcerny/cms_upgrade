from django.http import HttpResponse
import zmq
from zmq import ZMQError
import requests
from django.views.decorators.csrf import csrf_exempt
import logging
import tasks
from django.core.cache import cache
import time


context = zmq.Context()
sckt = context.socket(zmq.REQ)
sckt.connect("tcp://127.0.0.1:5555")


def index(request):
    return HttpResponse("i'm not talking to you.")


def zwget(request, controllerid):
    try:
        sckt.send("nix")
        sckt.recv()
    except ZMQError as e:
        return HttpResponse("zmqerror: %s" % e)
    return HttpResponse("done")


def zwset(request, controllerid):
    return HttpResponse("uh")


@csrf_exempt
def gw_tset_all(request, macaddr):

    if request.method == "POST":
        if 'data' in request.POST:
            r = requests.post("http://controme-main.appspot.com/set/%s/all/" % macaddr,
                              data={'data': request.POST['data']})
            return HttpResponse('done')

    return HttpResponse("", status=404)  # :-)


def gw_get_all(request, macaddr):
    tasks.get_gw_outputs.delay(macaddr)
    ret = cache.get("get_%s" % macaddr.lower())
    if ret:
        logging.warning("found %s in cache" % ret)
        return HttpResponse(ret)
    else:
        logging.warning("found nothing in cache")
        return HttpResponse("")


def gw_get_backup(request, macaddr, version):
    tasks.get_gw_backup.delay(macaddr, version)
    ret = cache.get("backup_%s" % macaddr.lower())
    if ret:
        return HttpResponse(ret)
    else:
        return HttpResponse("")


def gw_get_setting(request, macaddr, version):
    tasks.get_gw_setting.delay(macaddr, version)
    ret = cache.get("backup_%s" % macaddr.lower())
    if ret:
        return HttpResponse(ret)
    else:
        return HttpResponse("")


def get_nonce(request, macaddr):

    import base64
    try:
        from Crypto import Random
        nonce = Random.new().read(16)
    except ImportError:
        from os import urandom
        nonce = urandom(16)

    if request.method == "GET":  # gibt nonce zurueck
        nonce = base64.b64encode(nonce)
        _set_nonce(macaddr, nonce)

        logging.warning("returning b64nonce %s" % nonce)

        return HttpResponse('~' + nonce)

    elif request.method == "POST":  # verifiziert noncen, safe_mode fuer gateways
        # n = request.POST.get("nonce", None)
        # gw = Arduino.objects.cget(macaddr)
        # if n and _verify_nonce(macaddr, n):
        #     ret = "<1;%s>" % base64.b64encode(nonce)  # nehmen wir die ueberzaehlige nonce als padding her
        # else:
        #     ret = "<0;%s>" % base64.b64encode(nonce)  # nehmen wir die ueberzaehlige nonce als padding her
        # ret = gw.crypt_keys.encrypt(ret)
        #
        # return HttpResponse('~' + ret)
        return HttpResponse("")  # macht erst Sinn, wenn wir eine Datenbank haben hier


def _set_nonce(mac, nonce):
    n = cache.get("nonce_%s" % mac)
    now = int(time.time())
    if n:
        for _n, _t in n:
            if (now-_t) > 600:
                n.remove((_n, _t))
        n.append((nonce, now))
    else:
        n = [(nonce, now)]
    cache.set("nonce_%s" % mac, n)
    return True


def _verify_nonce(mac, nonce):
    # analog zu ch.verify_nonce()
    pass
