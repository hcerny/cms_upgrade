from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'dataslave.views.index'),
    url(r'^get/zwave/(?P<controllerid>([0-9A-Za-z_]{10}))/$', 'dataslave.views.zwget'),
    url(r'^set/zwave/(?P<controllerid>([0-9A-Za-z_]{10}))/$', 'dataslave.views.zwset'),
    (r'^set/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/all/', 'dataslave.views.gw_tset_all'),
    (r'^get/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/all/$', 'dataslave.views.gw_get_all'),
    (r'^get/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/(?P<version>\d+\.\d{2})/$', 'dataslave.views.gw_get_backup'),
    (r'^get/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/setting/(?P<version>\d+\.\d{2})/$', 'dataslave.views.gw_get_setting'),
    (r'^get/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/nonce/$', 'dataslave.views.get_nonce'),
)
